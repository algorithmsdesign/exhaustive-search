/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.exhaustivesearch;

/**
 *
 * @author Pattrapon N
 */
public class ExhaustiveSearch {
     public int maxProfit(int[] prices) {
        int minPriceSoFar = Integer.MAX_VALUE, maxProfitSoFar = 0;
        
        for (int i = 0; i < prices.length; i++) {
            if (minPriceSoFar > prices[i]) {
                minPriceSoFar = prices[i];
            } else {
                maxProfitSoFar = Math.max(maxProfitSoFar, prices[i] - minPriceSoFar);
            }
        }
        
        return maxProfitSoFar;
           
    }}
   